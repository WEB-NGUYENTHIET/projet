<!DOCTYPE html>
<!-- this page awaits a global variable $id_error -->
<html>
  <head>
    <meta charset="UTF-8">
    <title>Error</title>
    <link rel="stylesheet" href="css/general.css">
    <link rel="stylesheet" href="css/responsive.css">
  </head>
  <body>
  <?php
if ($error_id == 1) {
    echo "<h1>Vous n'avez pas sélectionné d'enregistrement</h1>";
} else if ($error_id == 2) {
    echo "<h1>L'enregistrement :  $invalid_id n'existe pas</h1>";
} else {
    echo "<h1>Erreur inconnue</h1>";
}
?>
  </body>
</html>
