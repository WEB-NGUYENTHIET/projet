<!DOCTYPE html>
<html>
<?php
// this page require session to be created
?>
<head>
    <meta charset="UTF-8">
    <title>Enregistrement de données</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/utils.js"></script>
    <script src="js/link_style.js"></script>
    <script src="js/record.js"></script>
    <script src="js/switchStateLoggedInAndOut.js"></script>
    <link rel="stylesheet" type="text/css" href="css/general.css">
    <link rel="stylesheet" type="text/css" href="css/record.css">
    <link rel="stylesheet" href="css/responsive.css">
</head>

<body data-id="<?php
if (isset($_SESSION['id'])) {
    echo $_SESSION['id'];
}
?>">

    <div id="account_management">

<div class="connected">
<div id="logged_in">
<div id="welcome"></div>
        <div id="logout">
            <form action="reponses/logout.php">
                    <button type="submit" name="ok">Se déconnecter</button>
            </form>
        </div>


        </div>
    </div>

        <div class="disconnected" id="login">
            <form action="reponses/login.php">
                <fieldset>
                    <legend>Connectez-vous</legend>
                    <input type="text" required="required" name="id"/>
                    <button type="submit" name="ok">Envoyer</button>
                </fieldset>
            </form>
        </div>

        <div class="disconnected" id="signup">
            <form action="reponses/signup.php">
                <fieldset>
                    <legend>Créez un compte</legend>
                    <input type="text" required="required" name="id"/>
                    <button type="submit" name="ok">Envoyer</button>
                </fieldset>
            </form>
        </div>

            </div>

 <button id="to_search" class="link">Client de recherche<span class="arrow"></span></button>


    <div id="recording_div">
    <p id="recording_title">Enregistrement de données</p>
    <p id="keyword_title">Gestion de mots clés</p>
    <input placeholder="Entrez un mot clé" id="keyword_input" type="text"/>
   <div id="param_keyword">
  <button id="add_keyword">Ajouter mot clé</button>
    <button id="clear_keyword">Réinitialiser mot clé</button>

   </div> 
      <div id="current_keywords">Mots clés actuels : </div>
    <button id="record">Démarrer l'enregistrement</button>

    <div id="recording_status"></div>
   </div>
   </body>

</html>
