<!DOCTYPE html>
<html>
<?php
// awaits an ids parameter, but can do without
?>
  <head>
    <meta charset="UTF-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/utils.js"></script>
    <script src="js/stat.js"></script>
    <script src="js/matrix.js"></script>
    <script src="js/3Dscene.js"></script>
    <link rel="stylesheet" href="css/general.css">
    <link rel="stylesheet" href="css/3Dscene.css">
    <link rel="stylesheet" href="css/responsive.css">
    <title>3D scene</title>
  </head>
  <body dataset-ids="<?php echo $ids ?>">
  <div id="param">
  <button id="play">Jouer le mouvement</button>
  <button id="reset">Réinitialiser</button>
  <button id="stop">Stop</button>
   <div id="global_param">
   <div id="coeff_space_container">
    <span>Coefficient </span> <input class="one" id="coeff_space" step="0.1" type="number" min="0.1" value="1"/>
    </div>
    <div id="rotation_world_container">
    <label for="rotate_world_x">Rotation du plan X</label>
      <input class="zero" step="0.1" value="0"type="number" name="rotate_world_x" id="rotate_world_x">
    <label for="rotate_world_x">Rotation du plan Y</label>
      <input class="zero" step="0.1" value="0"type="number" name="rotate_world_y" id="rotate_world_y">
    <label for="rotate_world_x">Rotation du plan Z</label>
      <input class="zero" step="0.1" value="0"type="number" name="rotate_world_z" id="rotate_world_z">
    </div>
    <div id="camera_settings">
      <div id="camera_position_container">
      <label for="camera_position_x">Camera X</label>
      <input class="zero"  type="number" step="1" value="0" name="camera_position_x" id="camera_position_x">
      <label for="camera_position_y">Camera Y</label>
      <input class="zero" type="number" step="1" value="0" name="camera_position_y" id="camera_position_y">
      </div>
      </div>

    </div>
    <div id="specific_param">
    </div>
    </div>


  <div id="3Dscene"></div>
  </body>
</html>
