<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Search</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/utils.js"></script>
    <script src="js/link_style.js"></script>
    <script src="js/search.js"></script>
    <link rel="stylesheet" href="css/general.css">
    <link rel="stylesheet" href="css/search.css">
    <link rel="stylesheet" href="css/responsive.css">

  </head>
  <body>

  <h1>Client de recherche</h1>
  <form id="search_form" action="reponses/search.php">
  <label for="user">Nom d'utilisateur</label>
    <select name="user" id="user">
    <option value="Invité">Invité</option>
    <?php
arrayToOptions($data->getAllUsers());
?>
    </select>
    <label for="keyword">Mot clé</label>
    <input type="text" list="keywords" name="keyword" id="keyword">
    <datalist id="keywords">
    <?php
$keywords = $data->getAllKeywords();
arrayToOptions($keywords);
?>
    </datalist>
<label for="begin_date">Rechercher aprés le</label>
    <input type="date" name="begin_date" id="begin_date">
<label for="end_date">Rechercher avant le</label>
    <input type="date" name="end_date" id="end_date">
    <button id="search" type="submit" value="ok">Chercher</button>
  </form>

  <div id="result_container"></div>
  <button class="link" id="visualization">Visualization  <span class="arrow"></span></button>
  </body>
</html>
