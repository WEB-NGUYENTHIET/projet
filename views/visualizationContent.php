<!DOCTYPE html>
<!-- this page awaits a global variable $sample and $ids-->
<html>
  <head>
    <meta charset="UTF-8">
    <title>Visualization</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/utils.js"></script>
    <script src="js/link_style.js"></script>
    <script src="js/stat.js"></script>
    <script src="js/visualization.js"></script>
    <link rel="stylesheet" type="text/css" href="css/general.css">
    <link rel="stylesheet" type="text/css" href="css/visualization.css">
    <link rel="stylesheet" href="css/responsive.css">
  </head>
  <body data-samples='<?php echo json_encode($samples) ?>' >
     <div id="param">
  <button id="to_3D_scene" class="link">Scene 3D <span class="arrow"></span></button>
    <div id="global_param">
    <span>Coefficient </span> <input id="coeff" step="1" type="number" min="1" value="10"/>

    <div id="smoothing">
      <label for="smoothing_enabled">Lissage activé </label> <input name="smoothing_enabled" type="checkbox" id="smoothing_enabled">
      <label for="smoothing_value">Taux de lissage ( pour LFP )</label>
      <input id="smoothing_value" step="1" type="number" min="1" value="30"/>
      <label for="only_display_smoothed_value">Afficher les valeurs lissées seulement</label>
      <input id="only_display_smoothed_value"type="checkbox" />
      <label for="smoothing_method">Methode de lissage</label>
      <select id="smoothing_method">
        <option value="lfp">LFP</option>
        <option value="mean3">mean-3</option>
      </select>
    </div>

    <div id="normalizing">
      <label for="normalizing_enabled">Normalisation activée</label> <input name="normalizing_enabled" type="checkbox" id="normalizing_enabled">
    </div>


    <div id="time_scale">
      <label for="time_scale_value">Facteur temps</label>
      <input id="time_scale_value" step="0.1" type="number" min="0.1" value="0.1"/>
    </div>


    </div>
    <div id="specific_param">
    </div>

    </div>
  <div id="visualization_container">
    <div id="orientation" class="div_visualization">
    </div>
    <div id="acceleration" class="div_visualization">
    </div>
    <div id="velocity" class="div_visualization">
    </div>
    <div id="position" class="div_visualization">
    </div>
  </div>


  </body>
</html>
