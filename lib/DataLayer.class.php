<?php
// This class allows for server data manipulation

class DataLayer
{

    /**
     * Return the JSON object corresponding to the data file name
     * @param   dataName    the name of the data file ( without the .json )
     */
    private function get($dataName)
    {
        return json_decode(file_get_contents(__DIR__ . "/../data/" . $dataName . ".json"), true);
    }

    /**
     * Write the $data as JSON object inside the $dataName file
     * @param   dataName    the name of the data file
     * @param   data        the data to be written ( a string )
     */
    private function put($dataName, $data)
    {
        file_put_contents(__DIR__ . "/../data/" . $dataName . ".json", json_encode($data));
    }

    /**
     * Connect a user. Will return null if user isn't found
     * @param   id  the id of the user
     */
    public function authenticate($id)
    {
        $data = $this->get("users");
        if (in_array($id, $data["users"])) {
            return $id;
        } else {
            return null;
        }
    }

    /**
     * Create a user, return null if the name already exists
     * @param   id  the id of the new user
     */
    public function createUser($id)
    {
        $data = $this->get("users");
        if (!in_array($id, $data["users"])) {
            $data["users"][] = $id;
            $this->put("users", $data);
            return $id;
        } else {
            return null;
        }

    }

    /** Add a recording
     * @param   entry    the entry
     */
    public function addRecording($entry)
    {
        $data = $this->get("recordings");
        $entry["id"] = uniqid();
        $data["recordings"][] = $entry;
        $this->put("recordings", $data);
    }

    /**
     * Retrieve an array of data ids
     */
    public function retrieveId()
    {
        $data = $this->get("recordings");
        $res = [];
        foreach ($data["recordings"] as $v) {
            $res[] = $v["id"];
        }
        return $res;
    }

    /**
     * Retrieve the recording which id is equal to given id as parameter, will return null if not found
     * @param   id  the id of the recording we want
     */
    public function getRecordingById($id)
    {
        $data = $this->get("recordings");
        $res = null;
        foreach ($data["recordings"] as $v) {
            if (strcmp($v["id"], $id) == 0) {
                $res = $v;
            }
        }
        return $res;
    }

    /**
     * Returns whether or not the ID exists in the database
     * @param   id  the id of the recording we want
     */
    public function idExists($id)
    {
        $data = $this->get("recordings");
        foreach ($data["recordings"] as $v) {
            if (strcmp($v["id"], $id) == 0) {
                return true;
            }
        }
        return false;

    }
    /**
     * Returns all users
     */
    public function getAllUsers()
    {
        $data = $this->get("users");
        return $data["users"];
    }

    /**
     * Returns all the keywords
     */
    public function getAllKeywords()
    {
        $res = [];
        $data = $this->get("recordings");
        foreach ($data["recordings"] as $v) {
            foreach ($v["keywords"] as $e) {
                if (!in_array($e, $res)) {
                    $res[] = $e;
                }
            }
        }

        return $res;
    }

    /**
     * Return the recordings ( as id, username, keywords, date and duration in ms ) that matches parameter user and keyword that is between begin date and end date
     * @param   user        the user that matches recordings
     * @param   keyword     one keyword that marches recording
     * @param   begin_date  match every recording after begin date
     * @param   end_date    match every recording before end date
     */
    public function search($user, $keyword, $begin_date, $end_date)
    {
        $res = [];
        $data = $this->get("recordings");
        if (!is_null($data["recordings"])) {
            foreach ($data["recordings"] as $v) {
                if ($user == "" || $user == $v["user"]) {
                    if ($keyword == "" || in_array($keyword, $v["keywords"])) {

                        $date = strtotime($v["date"]);
                        $b = strtotime($begin_date);
                        $e = strtotime($end_date);
                        if ($b == false || $b <= $date) {
                            if ($e == false || $date <= $e) {
                                if (isset($v['x']['interval'][0])) {
                                    $duration = end($v["x"]["interval"]) - $v["x"]["interval"][0];
                                    $res[] = [
                                        "id" => $v["id"],
                                        "user" => $v["user"],
                                        "keywords" => $v["keywords"],
                                        "date" => $v["date"],
                                        "duration" => $duration,
                                    ];
                                }
                            }
                        }
                    }
                }
            }
        }
        return $res;

    }
}
