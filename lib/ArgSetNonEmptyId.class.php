<?php
class ArgSetNonEmptyId extends AbstractArgumentSet{
    protected function definitions() {
        $this->defineNonEmptyString("id");
    }
}
?>