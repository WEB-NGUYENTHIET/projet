<?php
class ArgSetSearch extends AbstractArgumentSet
{
    protected function definitions()
    {
        $this->defineString("user");
        $this->defineString("keyword");
        $this->defineString("begin_date");
        $this->defineString("end_date");
    }
}
