<?php
class ArgSetNonEmptyData extends AbstractArgumentSet{
    protected function definitions() {
        $this->defineNonEmptyString("data");
    }
}
?>