<?php
// every php script under reponses/ directory will return a JSON object

// load any used class
spl_autoload_register(function ($className) {
    include ("{$className}.class.php");
});

// set the date
date_default_timezone_set('Europe/Paris');
header('Content-type: application/json; charset=UTF-8');

// create session
session_name('projet1nguyenthietwang');
session_start();

function produceError($message)
{
    echo answer(['status' => 'error', 'message' => $message]);
}

function produceResult($result)
{
    echo answer(['status' => 'ok', 'result' => $result]);
}

function answer($reponse)
{
    global $args;
    if (isset($args))
    {
        $reponse['args'] = $args->getValues();
    }
    else
    {
        $reponse['args'] = null;
    }
    echo json_encode($reponse);
}

$data = new DataLayer();
?>