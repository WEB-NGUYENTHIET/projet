<!-- this page awaits an $id parameter in get -->
<?php
require __DIR__ . "/lib/DataLayer.class.php";
$error_id;
$invalid_id;
$data = new DataLayer();

if (!isset($_GET["ids"])) {
    $error_id = 1;
    require __DIR__ . "/views/visualizationError.php";
    return;
}

$ids = $_GET["ids"];
$samples = [];
foreach($ids as $v) {
    $tmp = $data->getRecordingById($v);
    if (is_null($tmp)) {
        $invalid_id = $v;
        $error_id = 2;
        require __DIR__ . "/views/visualizationError.php";
        return;
    } else {
        array_push($samples,$data->getRecordingById($v));
    }
}
require __DIR__ . "/views/visualizationContent.php";
?>