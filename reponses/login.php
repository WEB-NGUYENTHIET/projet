<?php
require_once __DIR__ . "/../lib/reponse_commons.php";
$args = new ArgSetNonEmptyId();
try {
    if (isset($_SESSION['id'])) {
        produceError("Vous êtes déja connecté.");
        return;
    }

    if (!($args->isValid())) {
        produceError("Formulaire non valide.");
        return;
    }

    $personne = $data->authenticate($args->id);
    if ($personne == null) {
        produceError("Identifiant inconnu.");
        return;
    } else {
        $_SESSION['id'] = $personne;
        produceResult($personne);
        return;
    }
} catch (Exception $e) {
    produceError($e->getMessage());
    return;
}
