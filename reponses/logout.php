<?php
require_once __DIR__ . "/../lib/reponse_commons.php";
try {

    if (isset($_SESSION['id'])) {
        $res = $_SESSION['id'];
        unset($_SESSION['id']);
        produceResult($res);
        return;
    } else {
        produceError("Vous n'êtes pas connecté.");
        return;
    }
} catch (Exception $e) {
    produceError($e->getMessage());
    return;
}
