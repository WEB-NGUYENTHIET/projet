<?php
require_once __DIR__ . "/../lib/reponse_commons.php";
$args = new ArgSetSearch();
try
{
    if (!($args->isValid())) {
        produceError("Formulaire non valide.");
        return;
    }
    $res = $data->search($args->user, $args->keyword, $args->begin_date, $args->end_date);
    produceResult($res);
} catch (Exception $e) {
    produceError($e->getMessage());
    return;
}
