<?php
require_once __DIR__ . "/../lib/reponse_commons.php";
try {
    $args = new ArgSetNonEmptyData();
    $res = json_decode($_POST["data"], true);
    $data->addRecording($res);
    produceResult(true);
} catch (Exception $e) {
    produceError($e->getMessage());
    return;
}
