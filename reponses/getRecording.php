<?php
require_once __DIR__ . "/../lib/reponse_commons.php";
try
{
    $args = new ArgSetNonEmptyId();

    if (!($args->isValid())) {
        produceError("Formulaire non valide.");
        return;
    }

    $recording = $data->getRecordingByID($args->id);
    if ($recording == null) {
        produceError("Recording not found");
        return;
    } else {
        produceResult($recording);
        return;
    }
} catch (Exception $e) {
    produceError($e->getMessage());
    return;
}
