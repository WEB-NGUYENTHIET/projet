UP_DIR = /var/www/html/
PSUD_DIR = ~/public_html/

upload:
	sudo rm -rf $(UP_DIR)
	sudo mkdir -p $(UP_DIR)
	sudo cp -r ./* $(UP_DIR)
	sudo chmod o+w $(UP_DIR)data/users.json 
	sudo chmod o+w $(UP_DIR)data/recordings.json 

psud:
	rm -rf $(PSUD_DIR)
	mkdir -p $(PSUD_DIR)
	cp -r ./* $(PSUD_DIR)
	chmod o+w $(PSUD_DIR)data/users.json 
	chmod o+w $(PSUD_DIR)data/recordings.json 

debug:
	cat /var/log/apache2/error.log
