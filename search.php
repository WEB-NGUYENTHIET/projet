<?php
// chargement de bibliothéque
require_once __DIR__ . "/lib/DataLayer.class.php";
$data = new DataLayer();
require_once __DIR__ . "/views/lib/HTMLfunc.php";

// chargement de la page
require_once __DIR__ . "/views/searchContent.php";
