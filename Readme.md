# French

Ce projet consiste à implémenter un client d'enregsitrement et de visualization
des mouvements capturés grâce à l'accélérométre d'un smartphone. Ce projet a
été réalisé dans le cadre du cours intitulé " 
Application de l'informatique au monde socio-economique " enseigné par
[M.Vernier](https://perso.limsi.fr/vernier/)

# English

This project is about implementing a recording and visualization client for movement
using data sampled from an accelerometer of a smartphone. This project has 
been completed for a course with the translated title " Computer science concept 
applied to the socioeconomic world "
taught by [M.Vernier](https://perso.limsi.fr/vernier/)

# Instructions

Because of my tight schedule, the instructions are only available in english in
the current time. Instructions in French will be provided at a further date.

## Recording Client

![recording_client](figure1.png)

This is the screen of the recording client. This client features

- An overly simplistic account management system (1. and 2.)
- A link to the search client (3.)
- A button to start recording and attach keywords to the recordings (4. and 5.)

### Account management

The project allow for user to log in and log out. Being logged in allow for the
website to know which recordings has been recorded by who. It is not
mandatory to log in in order to record a movement.

The area indicated by **1** allows the user to input a pseudonym and log in into
that account.

The area indicated by **2** allows the user to create an account.

![logged_in](figure2.png)

Once the user is logged in, a message like so will display on the web page.

### Attaching keywords

The area indicated by **4** allows the user to attach keywords to a recording.
It is done by inputting a string, then clicking on *Ajouté mot clé*. This will 
add a keyword to the recording and it will be sent along with the recording.

It is possible to input as many keywords as one wishes.

By clicling on *Réinitialiser mot clé*, the set of keywords will be reset.

### Recording

The recording is done by clicking on the
big button called *Démarrer l'enregsitrement*. A countdown of 3 seconds will
start, and at the end of that countdown, the recording will start. The user can
move around and it will sample the data from the phone's accelerometer.

The user can stop at any time by clicking on the same button again.


![recording_sent](figure3.png)

This message will display if the recording has been sent succesfully.

## Research client

The user can access the research client by clicking on the button indicated
by area **3** of the previous client.

![research_client](figure4.png)

The research client allow for user to 
- parameter a query (1)
- search the query (2)
- look up the meta data of a recording (3)
- visualize selected recordigns (4)

### Query Parameters (1)

The queries can have a total of 4 criterias. From top to bottom
- The author of the recording
- A matching keyword
- A lower bound date
- A upper bound date

### Search the database (2)

Let *a*, *kw*, *date_start* and *date_end* be, respectively,
the author, keyword, lower bound date and upper bound date
of our query parameters. Once the client hit the button *Chercher*,
the server will return a list of all recordings for which

- the author is *a*
- at least one keyword is *kw*
- the recording has been sent after *date_start*
- the recording has been sent before *date_end*

### Query results (3)

From top to bottom, the information displayed are

- the author of the recording
- the keywords associated with the recording
- the duration of the recording
- the date at which the recording has been sent
- a checkbox for visualization

### Selecting the recording to visualize (4)

Clicking on the *Visualization* button will start the visualization client
for all the recordings that has been selected (by ckecking the checkbox)

## 2D Visualization 

![research_client](figure5.png)

This client allow to visualize 0, 1 or more recordings. 
The user can adjust global parameters that are applied to all recordings,
or specific paramaters that are applied to one recording.

### Global parameters (**1**)

From top to bottom :

#### Coefficient

The coefficient applies a multiplying factor to all recordings. In short
the higher this values it, the bigger vertically
the recordings will be.

#### Smoothing

This parameters allows to smooth the values. It is
possible to use the mean, for each sample at time *t*, of the value of the sample
at time *t-1* and *t+1*. It is also possible to use LFP to smooth the values.
With LFP method it is possible to input a smoothing value. The greater is the
smoother the recordings will be.

#### Normalizing

It is possible to normalize the recordings. They will all fit within the canvas.

#### Time factor

The time factor allows to stretch horizontally the recordings.

### Individual parameters (**2**)

Each recordings can be parameterized individually

From top to bottom : 

#### Time offset

Input a number to offset the recording by *x* ms. Visually it will move the
recording to the right if *x > 0*

#### Color set

The user can change the color for each axis of the recording.

### What do all these lines means? (**3**)

The recordings are split between 9 graphs. From top to bottom, left to right,
orientation, acceleration, velocity, position on axis X,
orientation, acceleration, velocity, position on axis Y,
orientation, acceleration, velocity, position on axis Z.

### Visualize the recordings on 3 dimensions

Click on the link in area **4** to go to the 3D scene

## 3D Scene

![research_client](figure6.png)

This client allows you to check out your recordings in 3 dimensions.

### Play the recording (**1**)

Click on *Joueur le mouvement* and the movement will play. It will try to
reproduce the movements a user made with its phone.

Click on *stop* to stop the recording.

Click on *réinitialiser* to reset the parameters

### Global parameters (**2**)

Coefficient is a multiplying factor on the coordinates of the recordings.

You can rotate the recording in 3 axis.

You can offset the camera on two axis.

### Individual parameters (**3**)

For each recordings, it is possible to : 
- change its color
- offset the recording on 3 axis
- scale the recording on 3 axis
- offset the recording's timing 
- scale the recording's timing

## Contact

nguyenthiet.lam@gmail.com















