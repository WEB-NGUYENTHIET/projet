var width = 200;
var height = width;
var nb_field = 4; // number of field for each axis ( acc, orientation etc.. )
var data_size = 3 * nb_field; // total size of the sample we will be working with
var init_colors = []; // initial color values
var origin_samples = []; // untouched origin sample

window.addEventListener("load", function (ev) {

    // create the canvases
    createCanvases();

    // add function to allow for interval measuring on the graphs
    measurableInterval();

    // transform the raw data into workable data
    fillOriginSamples();

    // create the initial colours
    fillInitColors();

    // create html options for each samples
    createOptionPerSample(origin_samples);

    // check conflicting params;
    checkConflictingParams();

    // visualize all the sample
    visualizeRecordingMain(origin_samples);
    $("input, select").each(function (i, e) {
        $(this).change(function () {
            // clear all canvases
            $("canvas").each(function (i, e) {
                var ctx = e.getContext("2d");
                ctx.clearRect(0, 0, e.width, e.height);
            });

            // check conflicting params
            checkConflictingParams();

            // visualize all the sample with new parameters taken into account
            visualizeRecordingMain(origin_samples);
        });
    });

    // link to 3D scene
    $("#to_3D_scene").click(function (ev) {
        localStorage.setItem("samples", JSON.stringify(origin_samples));
        document.location.href = "3Dscene.php";
    });
});

// this function will check parameter that conflict with themselves, and disable the one that are
function checkConflictingParams() {
    // first enabled all param
    $("input").prop("disabled", false);
    $("select").prop("disabled", false);

    // if normalizing is enabled, then coefficient is ignored
    if ($("#normalizing_enabled").is(":checked")) {
        $("#coeff").prop('disabled', true);
    }

    // only display smoothed value and smoothing value is enabled only if smoothing is enabled
    if (!($("#smoothing_enabled").is(":checked"))) {
        $("#smoothing_value").prop('disabled', true);
        $("#only_display_smoothed_value").prop('disabled', true);
        $("#only_display_smoothed_value").prop('checked', false);
        $("#smoothing_method").prop('disabled', true);
    } else {
        if ($("#smoothing_method").val() != "lfp") {
            $("#smoothing_value").prop('disabled', true);
        }
    }

}


// when a user holds down the button on the mouse 
// on a graph he can measure the time between the
// point where he holds down the button and the point where
// he releases the button
function measurableInterval() {
    // coordinate to display the measured interval
    let y;

    // function on mouseup
    let f;

    // event handler on mouse down
    $("canvas.overlay").mousedown(function (ev) {

        // fetch the coordinate relative to the canvas
        x1 = ev.offsetX;
        y = ev.offsetY;

        // event handler on mouseup
        this.addEventListener("mouseup", f = function (ev) {
            // fetch the coordinate
            x2 = ev.offsetX;

            let b = Math.max(x1, x2);
            let a = Math.min(x1, x2);

            // fetch the coefficient in case of scaled time
            let coeff = $("#time_scale_value").val();

            // measured interval
            let total = Math.floor((b - a) / coeff);

            // draw on the canvas
            let ctx = this.getContext("2d");

            // reset the canvas
            ctx.clearRect(0, 0, width, height);

            // display the measured interval
            ctx.font = "30px Arial";
            ctx.fillStyle = "#000000";
            ctx.fillText(total.toString() + " ms", a, y);

            // draw a line beneath it
            ctx.beginPath();
            ctx.strokeStyle = "#000000";
            ctx.moveTo(a, y - 30);
            ctx.lineTo(b, y - 30);
            ctx.stroke();

            // clear this event
            this.removeEventListener("mouseup",f);
        });
    });
}

// fill origin sample, which is workable data
function fillOriginSamples() {
    $("body").data("samples").forEach(function (e, i) {
        origin_samples.push(createDataArray(e));
    });
}

// create the options for each sample
function createOptionPerSample(samples) {
    samples.forEach(function (e, i) {
        // more user friendly to begin indexes with 1
        let num = i + 1;

        // create the containing div
        let div = document.createElement("div");
        div.id = "courbe_" + i;

        // create the div that countains the shifting option
        let shift = document.createElement("div");
        shift.id = "shift_" + i;

        // create the div that contains the scaling option
        let time_scale = document.createElement('div');
        time_scale.id = "time_scale_container_" + i;

        // create the div that countains the option to change colours
        let color = document.createElement("div");
        color.id = "color_" + i;

        // create option for shifting
        shift.innerHTML = "Courbe " + num + "<br/> Décaler le temps de <input type=\"number\" step=\"10\" value=\"0\" class=\"shifting_values\"/> ms";

        // create option for time scale
        time_scale.innerHTML = 'Facteur temps <input id="time_scale_' + i + '"type="number" min="0.1" step="0.1" value="1"/>';
        // create option for color
        color.innerHTML = "Couleur X <input type=\"color\" value=\"" + init_colors[0][i] + "\"id=\"color_x_" + i + "\"/>";
        color.innerHTML += "Couleur Y <input type=\"color\" value=\"" + init_colors[1][i] + "\"id=\"color_y_" + i + "\"/>";
        color.innerHTML += "Couleur Z <input type=\"color\" value=\"" + init_colors[2][i] + "\"id=\"color_z_" + i + "\"/>";
        color.innerHTML += "Couleur lissé <input type=\"color\" value=\"" + init_colors[3][i] + "\"id=\"color_smoothed_" + i + "\"/>";



        // dom tree assembling
        div.appendChild(shift);
        div.appendChild(time_scale);
        div.appendChild(color);
        $("#specific_param").append(div);
    });
}

// create the initial colors for all samples
function fillInitColors() {

    // n is the number of samples
    let n = origin_samples.length;

    // colours for 3 dimensional axis
    for (let i = 0; i < 3; i++) {
        let tmp = [];
        for (let j = 120 * i; j < 120 * (i + 1); j += 120 / n) {
            tmp.push(HUEtoRGB(j, 0.9, 0.9));
        }
        init_colors.push(tmp);
    }

    // colour for smoothed overlay
    init_colors[3] = [];
    for (let i = 1; i < n + 1; i++) {
        init_colors[3].push(HUEtoRGB(0, 0, i / (n + 1)));
    }

}


// concat working data ( samples ) into one array of value
// samples : all of samples
// i : the index of the field we want to concat
// nb_axis : 1 if we just want the first axis, 2 if we also want the second, 3 if we want all three axis, a fourth axis is not yet implemented
function combineSample(samples, i, nb_axis) {
    let res = [];
    samples.forEach(function (e) {
        res = res.concat(e[i].value);
        for (let j = 1; j < nb_axis; j++) {
            res = res.concat(e[i + j].value);
        }
    });
    return res;
}

// create the normalizing functions for all samples
function createNormFuns(samples) {
    var res = [];
    // create the normalizing functions for orientation sample
    for (let i = 0; i < 3; i++) {
        // the tmp_sample is a sample that contains all values of each samples in
        // one array
        let tmp_sample = combineSample(samples, i, 1);
        res[i] = createNormalizeFunction(tmp_sample, height);
    }

    // create the normalizing functions for acceleration, position and velocity
    for (let i = 1; i < 4; i++) {
        let tmp_sample = combineSample(samples, i * 3, nb_field - 1);

        let tmp_norm_fun = createNormalizeFunction(tmp_sample, height);
        for (let j = 0; j < nb_field - 1; j++) {
            res[i * 3 + j] = tmp_norm_fun;
        }
    }
    return res;
}

// create the canvases
function createCanvases() {
    // name of the axis
    var axis;

    // title of the canvas
    var title;

    // create containers for each axis 
    for (var i = 0; i < 3; i++) {
        $(".div_visualization").each(function (j, e) {
            axis = axisSelector(i, "X", "Y", "Z");
            title = axis + ' ' + e.id;
            e.innerHTML = e.innerHTML + '<div class="' + axis + '_container"><span class="label_visualization">' + title + '</span><div class="canvas_container"></div>';
        });
    }

    // add the canvases into the container
    $("div.canvas_container").append(
        '<canvas class="graph" width="' + width + '" height="' + height + '"></canvas>' +
        '<canvas class="overlay" width="' + width + '" height="' + height + '"></canvas>'
    );

    // fit the container 
    $("div.canvas_container").css({
        'width': width,
        'height': height
    });
}

// create a workable data 
// indexes are : ( will be used for any other array that refers to these sample )
// 0 : pitch
// 1 : roll
// 2 : yaw
// 3 - 5 : acceleration of x y z
// 6 - 8 : velocity of x y z
// 9 - 11 : position of x y z
// in general index mod 3 = 0 is for x, 1 is for y and 2 is for z
function createDataArray(t) {
    var data_array = [];

    // orientation
    data_array[0] = t.pitch;
    data_array[1] = t.roll;
    data_array[2] = t.yaw;

    // acceleration
    data_array[3] = t.x;
    data_array[4] = t.y;
    data_array[5] = t.z;

    // compute velocity and position
    for (let i = 6; i < data_size; i++) {
        data_array[i] = sum_integral(data_array[i - 3]);
    }

    return data_array;
}

// create a workable sample
function sampleCreate(v, i) {
    return {
        "value": v,
        "interval": i
    };
}

// create an object that contains all of the global parameters
function fetchGlobamParam(samples) {
    var res = {
        "normalizing_enabled": $("#normalizing_enabled").is(":checked"),
        "smoothing_enabled": $("#smoothing_enabled").is(":checked"),
        "only_display_smoothed_values": $('#only_display_smoothed_value').is(":checked"),
        "norm_funs": createNormFuns(samples),
        "smoothing_method": $("#smoothing_method").val(),
        "smoothing": $("#smoothing_value").val(),
        "coeff_y": parseFloat($("#coeff").val()),
        "time_scale_value": parseFloat($("#time_scale_value").val())
    }
    return res;
}
// create an object that contain specific parameters for each sample
// i is the index in the main samples array of the specific sample we want to configure
function fetchSpecificParam(i) {
    var res =
    {
        // color
        "colorx": $("#color_x_" + i).val(),
        "colory": $("#color_y_" + i).val(),
        "colorz": $("#color_z_" + i).val(),
        "colorsmoothed": $("#color_smoothed_" + i).val(),

        // shift in time
        "shift": parseInt($("#shift_" + i + " input").val()),

        // scale in time
        "time_scale": parseFloat($("#time_scale_" + i).val())
    }
    return res;
}

// main function that will visualize each sample
function visualizeRecordingMain(samples) {
    // fetch the global parameters
    let global_param = fetchGlobamParam(samples);
    samples.forEach(function (e, i) {
        // fetch the specific parameters
        let specific_param = fetchSpecificParam(i);
        visualizeRecording(
            e,
            specific_param,
            global_param
        );
    });
}

// make a call to plot for each axis, and also do some preprocessing
function visualizeRecording(t, specific_param, global_param) {
    // smoothing method
    var smooth;
    if (global_param.smoothing_method == "lfp") {
        smooth = LFP;
    } else if (global_param.smoothing_method == "mean3") {
        smooth = mean3;
    } else {
        smooth = LFP;
    }
    // make a copy
    var data_array = copy(t);
    // the middle of the height
    let middle_height = height / 2;
    // constant zero
    let zero = new Array();

    // preprocessing
    data_array.forEach(function (e, i) {

        // modification on y axis
        // normalize
        if (global_param.normalizing_enabled) {
            e.value = normalize(e.value, global_param.norm_funs[i]);
            zero[i] = global_param.norm_funs[i](0);
        } else {
            let coeff_y = global_param.coeff_y;
            e.value.forEach(function (x, j) {
                e.value[j] = (x * coeff_y) + middle_height;
            });
            zero[i] = middle_height;
        }

        // modification on x axis
        // scale the timestamps to fit the drawing
        let coeff_x;
        coeff_x = specific_param.time_scale * global_param.time_scale_value;

        e.interval.forEach(function (x, j) {
            e.interval[j] = (specific_param.shift + x) * coeff_x;
        });
    });

    if (!global_param.only_display_smoothed_values) {
        // plotting
        $("canvas.graph").each(function (i, e) {
            let color = axisSelector(i, specific_param.colorx, specific_param.colory, specific_param.colorz);
            plot(e, data_array[i], color, zero[i]);
        })
    }

    // if smoothing is enabled, smoothes the values
    if (global_param.smoothing_enabled) {

        // if only the smoothed values are displayed, we will display only the smooothed values with their respective colors
        if (global_param.only_display_smoothed_values) {
            $("canvas.graph").each(function (i, e) {
                let color = axisSelector(i, specific_param.colorx, specific_param.colory, specific_param.colorz);
                plot(e, smooth(data_array[i], global_param.smoothing), color, zero[i]);
            });
            // otherwise we display them alongside the non smoothed value in a different colour
        } else {
            $("canvas.graph").each(function (i, e) {
                plot(e, smooth(data_array[i], global_param.smoothing), specific_param.colorsmoothed, zero[i]);
            });
        }

    }
}

// draw a line at height 0
function lineZero(x, y, ctx) {
    ctx.beginPath();
    ctx.moveTo(0, y);
    ctx.lineTo(x, y);
    ctx.strokeStyle = "#000000";
    ctx.stroke();
}

// plot a single axis
function plot(cv, data, color, zero) {
    // context
    let ctx = cv.getContext("2d");
    lineZero(width, zero, ctx);
    ctx.beginPath();

    // define color
    ctx.strokeStyle = color;

    // move to the middle left
    ctx.moveTo(data.interval[0], data.value[0]);

    // plot data
    data.value.forEach(function (e, i) {
        ctx.lineTo(data.interval[i], e);
        ctx.stroke();
    });
}