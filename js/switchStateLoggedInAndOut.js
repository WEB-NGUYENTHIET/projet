// this script manage log in / log out / sign up queries and ma,nages the state of the website from a logged in state to 
// a logged out state and vice versa

window.addEventListener("load", function (event) {
    // on first load, check if user is logged in or not
    var res = $("body").data("id");
    if (res.length > 0) {
        toLoggedIn(res);
    } else {
        toLoggedOut();
    }

    // manages login and sign up
    $("#login form, #signup form").submit(function (ev) {
        ev.preventDefault();
        ajaxSendForm(this, "GET",
            function (answer) {
                toLoggedIn(answer);
            },
            function (answer) {
                window.alert(answer);
            });
    })

    // manage logout
    $("#logout form").submit(function (ev) {
        ev.preventDefault();
        ajaxSendForm(this, "GET",
            function (answer) {
                toLoggedOut(answer);
            }, function (answer) {
                window.alert(answer);
            })
    })


});

// switch the state of the website to logged in
function toLoggedIn(id) {
    $(".disconnected").css("display", "none");
    $(".connected").css("display", "initial");
    $("#welcome").text("Bonjour " + id)
    $("body").attr("data-id",id);
}

// switch the state of the website to logged out
function toLoggedOut() {
    $(".disconnected").css("display", "initial");
    $(".connected").css("display", "none");
    $("body").attr("data-id","");
}