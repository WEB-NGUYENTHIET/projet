// this script add some animation for stylized links
window.addEventListener("load", function (ev) {
    $(".arrow").text("➜");

    $(".link").hover(function (ev) {
        $(".arrow").css({
            "opacity": "1",
            "left": "0px"
        });
    },
        function (ev) {
            $(".arrow").css({
                "opacity": "0",
                "left": "-20px"
            });
        })
});