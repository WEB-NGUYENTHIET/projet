// matrix computation

// multiplication
function matrixMult(a, b) {
    let res = new Array();

    let height = a.length;
    let common = a[0].length;
    let width = b[0].length;

    for (let i = 0; i < height; i++) {
        // the line
        let tmp = new Array();

        // fill the line
        for (let j = 0; j < width; j++) {
            let tmp_val = 0;
            for (let k = 0; k < common; k++) {
                tmp_val += a[i][k] * b[k][j];
            }
            tmp.push(tmp_val);
        }

        // add the line to the result matrix
        res.push(tmp);
    }
    return res;
}

// addition
function matrixAdd(a, b) {
    let res = new Array();

    a.forEach(function(e1,i) {
        let tmp = new Array();
        e1.forEach(function(e2,j) {
           tmp.push(e2+b[i][j]);
        });
        res.push(tmp);
    })
    return res; 
}