window.addEventListener('load', function (ev) {
    $('form').submit(function (ev) {
        // prevent loading new page
        ev.preventDefault();

        // check validity
        let b = $("#begin_date").val();
        let e = $("#end_date").val();
        if (b > e) {
            window.alert("Date de début doit être avant date de fin");
        } else {
            ajaxSendForm(this, "GET", displayResultSearch, displayErrorSearch)
        }
    });

    // link to visualization client
    $('#visualization').click(function (ev) {

        // get all the ids to visualize
        let ids = new Array();
        $('.visualization_selector').each(function (i, e) {
            if (e.checked) {
                ids.push(e.dataset.id);
            }
        });

        // create the url
        let new_url = "./visualization.php?";

        ids.forEach(function (e) {
            new_url += "ids[]=" + e + "&";
        });

        // go to the new url
        location.href = new_url;
    });
});

// fill the result container with the results in html
function displayResultSearch(result) {

    // get the container
    let div = document.getElementById("result_container");

    // delete anything inside
    div.innerHTML = "";

    if (result.length > 0) {
        // iterate over the different recording found
        result.forEach(function (e) {
            let res = recordingToSearchresult(e);
            div.appendChild(res);
        })
    } else {
        div.textContent = "Aucun résultat";
    }
}

// create a html element that represents a recording
function recordingToSearchresult(recording) {

    // create the divs
    let div = document.createElement('div');
    let user = document.createElement('div');
    user.classList.add("user");
    let keywords = document.createElement('div');
    keywords.classList.add("keywords");
    let date = document.createElement('div');
    date.classList.add("date");
    let duration = document.createElement('div');
    duration.classList.add("duration");

    let visualizationSelect = document.createElement("div");
    let label_checkbox = document.createElement("label");
    label_checkbox.textContent = "Sélectionner pour la visualisation";
    let id_checkbox = document.createElement('input');
    id_checkbox.type = "checkbox";
    id_checkbox.classList.add("visualization_selector");
    visualizationSelect.appendChild(label_checkbox);
    visualizationSelect.appendChild(id_checkbox);


    // make the dom tree
    div.appendChild(user);
    div.appendChild(keywords);
    div.appendChild(duration);
    div.appendChild(date);
    div.appendChild(visualizationSelect);

    // fill the content
    duration.textContent = recording.duration + " ms";
    user.textContent = recording.user;
    keywords.textContent = "";
    recording.keywords.forEach(function (e) {
        keywords.textContent += e + " ";
    })
    date.textContent = recording.date;
    id_checkbox.dataset.id = recording.id;

    // return
    return div;
}

function displayErrorSearch(result) {
    window.alert("Une erreur est survenue, veuillez réessayer ultérieurement.");
}