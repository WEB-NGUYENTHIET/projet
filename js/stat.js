// some stats and math function

// create a normalizing function for a sample t, size is the size of the graph
function createNormalizeFunction(t, size) {
    var max = Math.max.apply(null, t);
    var min = Math.min.apply(null, t);

    return function (e) {
        return (size * (e - min)) / (max - min);
    }

}

// normalize the data to fit the canvas
function normalize(t, f) {
    var i = 0;
    var res = [];
    t.forEach(function (e) {
        res[i] = f(e);
        i++;
    })
    return res;
}

// integration tool
function sum_integral(t) {
    var res = {
        "value": new Array(),
        "interval": new Array()
    }

    // we will assume initial value is 0
    res.value[0] = 0;
    res.interval[0] = t.interval[0];

    len = t.value.length;
    for (var i = 1; i < len; i++) {
        res.value[i] = res.value[i - 1] + ((t.value[i] + t.value[i - 1]) / 2) * (t.interval[i] - t.interval[i - 1]) / 1000;

        // keep the time values
        res.interval[i] = t.interval[i];
    }
    return res;
}

// low filter pass
// heavily inspired/taken from : http://phrogz.net/js/framerate-independent-low-pass-filter.html
function LFP(t, smoothing) {
    var res = {
        "value": new Array(),
        "interval": new Array()
    }
    res.value[0] = t.value[0];
    res.interval[0] = t.interval[0];
    var value = t.value[0]; // start with the first input
    for (var i = 1, len = t.value.length; i < len; ++i) {
        var currentValue = t.value[i];
        value += (currentValue - value) / (smoothing / (t.interval[i] - t.interval[i - 1]));
        res.value[i] = value;
        res.interval[i] = t.interval[i];
    }
    return res;
}

// smooth a sample by using the mean 3 method : for each value indexed by i, it is replaced by the mean of value indexed by i, i+1 and i-1
function mean3(t) {

    // the result
    var res = {
        "value": new Array(),
        "interval": new Array()
    };

    // the first value isn't changed
    res.value[0] = t.value[0];
    res.interval[0] = t.interval[0];

    // compute the mean
    for (var i = 1, len = t.value.length - 1; i < len; ++i) {
        res.interval[i] = t.interval[i];
        res.value[i] = (t.value[i - 1] + t.value[i] + t.value[i + 1]) / 3;
    }
    // the last value isn't changed
    res.value[i] = t.value[i];
    res.interval[i] = t.interval[i];

    // return the result
    return res;
}