// original samples
var origin_samples;
// constantes
var height = 600;
var width = height;
var middle_height = height / 2;
var middle_width = middle_height;

// interval for animation
var interval;

window.addEventListener("load", function () {
    // retrieve the data array from previous page
    origin_samples = JSON.parse(localStorage.getItem("samples"));

    // create the canvas
    $("#3Dscene").append("<canvas width=" + width + " height=" + height + "></canvas>");

    // generate specific param for each sample
    createSpecificParam(origin_samples);

    // represent all movement
    representation(origin_samples);

    // updating representation
    $("#param input").change(updateDrawing);

    // reset button
    $("#reset").click(function (ev) {

        // stop any animation
        stopPlay();

        // reset all the parameters
        $("input.zero").val(0);
        $("input.one").val(1);

        // draw
        updateDrawing(ev);
    });

    $("#stop").click(function (ev) {
        stopPlay();
    });
})

// clear the canvas and draw with the new parameters in consideration
function updateDrawing(ev) {
    // clear the canvas
    let cv = $("canvas").get(0);
    let ctx = cv.getContext("2d");
    ctx.clearRect(0, 0, height, width);

    // redraw all the samples
    representation(origin_samples);
}

// create parameters for each sample
function createSpecificParam(t) {
    // container div
    let parent = $("#specific_param").get(0);

    t.forEach(function (e, i) {
        // div for each sample
        let div = document.createElement("div");
        div.id = "specific_param_" + i;
        div.textContent = "Parametre pour courbe " + (i + 1);

        // param color
        let div_color = document.createElement("div");
        let label_color = document.createElement("label");
        label_color.textContent = "Couleur";
        let input_color = document.createElement("input");
        input_color.id = "color_" + i;
        input_color.type = "color";
        input_color.value = getRandomColor();

        div_color.appendChild(label_color);
        div_color.appendChild(input_color);


        // param space shift
        let div_space_shift = document.createElement("div");
        div_space_shift.id = "space_shift_" + i;

        // param space scale
        let div_space_scale = document.createElement("div");
        div_space_scale.id = "space_scale_" + i;


        for (let j = 0; j < 3; j++) {
            let axe = axisSelector(j, "x", "y", "z");

            let label_shift = document.createElement("label");
            label_shift.textContent = "Decalage de l'axe " + axe;
            let input_shift = document.createElement("input");
            input_shift.id = "space_shift_" + axe + "_" + i;
            input_shift.type = "number";
            input_shift.value = 0;
            input_shift.step = 1;
            input_shift.classList.add("zero");

            div_space_shift.appendChild(label_shift)
            div_space_shift.appendChild(input_shift)

            let label_space_scale = document.createElement("label");
            label_space_scale.textContent = "Facteur axe " + axe;
            let input_space_scale = document.createElement("input");
            input_space_scale.id = "space_scale_" + axe + "_" + i;
            input_space_scale.type = "number";
            input_space_scale.value = 1;
            input_space_scale.min = 0.1;
            input_space_scale.step = 0.1;
            input_space_scale.classList.add("one");

            div_space_scale.appendChild(label_space_scale)
            div_space_scale.appendChild(input_space_scale)
        }

        // param time shift
        let div_time_shift = document.createElement("div");
        div_time_shift.id = "time_shift_container_" + i;
        let label_time_shift = document.createElement("label");
        label_time_shift.textContent = "Decalage du temps";
        let input_time_shift = document.createElement("input");
        input_time_shift.id = "time_shift_" + i;
        input_time_shift.type = "number";
        input_time_shift.value = 0;
        input_time_shift.step = 1;
        input_time_shift.classList.add("zero");

        div_time_shift.appendChild(label_time_shift);
        div_time_shift.appendChild(input_time_shift);

        // param time scale
        let div_time_scale = document.createElement("div");
        div_time_scale.id = "time_scale_container_" + i;
        let label_time_scale = document.createElement("label");
        label_time_scale.textContent = "Facteur temps";
        let input_time_scale = document.createElement("input");
        input_time_scale.id = "time_scale_" + i;
        input_time_scale.type = "number";
        input_time_scale.value = 1;
        input_time_scale.step = 1;
        input_time_scale.min = 1;
        input_time_scale.max = 5;
        input_time_scale.classList.add("one");

        div_time_scale.appendChild(label_time_scale);
        div_time_scale.appendChild(input_time_scale);

        div.appendChild(div_color);
        div.appendChild(div_space_shift);
        div.appendChild(div_space_scale);
        div.appendChild(div_time_shift);
        div.appendChild(div_time_scale);
        parent.appendChild(div);
    });

}

// fetch the param that are global
function fetchGlobalParam() {
    return {
        // coefficient in space
        "coeff_space": parseFloat($("#coeff_space").val()),
        // rotation world in space
        "rotate_world_x": parseFloat($("#rotate_world_x").val()),
        "rotate_world_y": parseFloat($("#rotate_world_y").val()),
        "rotate_world_z": parseFloat($("#rotate_world_z").val()),
        // camera position
        "camera_position_x": parseFloat($("#camera_position_x").val()),
        "camera_position_y": parseFloat($("#camera_position_y").val())
    }
}

// fetch param specific to each sample
function fetchSpecificParam(i) {
    return {
        // shift in space for x, y, z
        "space_shift_x": parseFloat($("#space_shift_x_" + i).val()),
        "space_shift_y": parseFloat($("#space_shift_y_" + i).val()),
        "space_shift_z": parseFloat($("#space_shift_z_" + i).val()),
        // scale in space for x, y, z
        "space_scale_x": parseFloat($("#space_scale_x_" + i).val()),
        "space_scale_y": parseFloat($("#space_scale_y_" + i).val()),
        "space_scale_z": parseFloat($("#space_scale_z_" + i).val()),
        // color
        "color": $("#color_" + i).val(),
        // shift in time
        "time_shift": parseFloat($("#time_shift_" + i).val()),
        // scale in time
        "time_scale": 1 / parseFloat($("#time_scale_" + i).val())
    }
}

function discardUselessData(sample) {
    return {
        "rotation": {
            // rotation
            "x": sample[0],
            "y": sample[1],
            "z": sample[2],
        },
        "position": {
            // coordinates
            "x": sample[9],
            "y": sample[10],
            "z": sample[11]
        }
    }
}

// preprocess a sample for graphical representation, will return an array of coordinates, with time t 
function preprocessing(sample, global_param, specific_param) {
    let res = copy(sample);

    // iterator over res
    let i = 0;

    // scaling / shifting
    for (var key in res.position) {

        // t is the sample we will preprocess
        let t = res.position[key];
        t.value.forEach(function (e, j) {

            // select the right modifier for the right axis
            let shift = axisSelector(i,
                specific_param.space_shift_x,
                specific_param.space_shift_y,
                specific_param.space_shift_z,
            );

            let coeff = axisSelector(i,
                specific_param.space_scale_x,
                specific_param.space_scale_y,
                specific_param.space_scale_z
            );

            // change the coordinates
            t.value[j] = (e * global_param.coeff_space * coeff) + shift;
            // change the time
            t.interval[j] = t.interval[j] + specific_param.time_shift;
            t.interval[j] *= specific_param.time_scale;
            t.interval[j] = Math.round(t.interval[j]);
        });
        i++;
    };

    // angles
    let beta = global_param.rotate_world_x;
    let gamma = global_param.rotate_world_y;
    let alpha = global_param.rotate_world_z;

    // rotate
    rotation(res.position, beta, gamma, alpha);

    return res;
}

// rotate the x y and z coordinate inside t by angle beta(x), gamma(y) and alpha(z);
// modify in place
// rotation matrix : https://en.wikipedia.org/wiki/Rotation_matrix
function rotation(t, beta, gamma, alpha) {

    // sin cos
    let cos_beta = Math.cos(beta);
    let sin_beta = Math.sin(beta);

    let cos_gamma = Math.cos(gamma);
    let sin_gamma = Math.sin(gamma);

    let cos_alpha = Math.cos(alpha);
    let sin_alpha = Math.sin(alpha);

    // compute the matrices
    let m1 = [
        [1, 0, 0],
        [0, cos_beta, -sin_beta],
        [0, sin_beta, cos_beta]
    ];

    let m2 = [
        [cos_gamma, 0, sin_gamma],
        [0, 1, 0],
        [-sin_gamma, 0, cos_gamma]
    ];

    let m3 = [
        [cos_alpha, -sin_alpha, 0],
        [sin_alpha, cos_alpha, 0],
        [0, 0, 1]
    ];

    let rotation_matrix = matrixMult(m1, matrixMult(m2, m3));

    // apply the rotation for each value
    t.x.value.forEach(function (e, i) {
        let vector = [
            [e, t.y.value[i], t.z.value[i]]
        ];

        let res_m = matrixMult(vector, rotation_matrix);

        // update the value
        t.x.value[i] = res_m[0][0];
        t.y.value[i] = res_m[0][1];
        t.z.value[i] = res_m[0][2];
    });
}

// main function that will transform a set of data into a representation in 2D
function representation(t) {
    // global param fetching
    let global_param = fetchGlobalParam();

    // data used for animation because they need to be simultaneous
    let all_plot_data = new Array();
    let all_colors = new Array();

    t.forEach(function (e, i) {
        // specific param for each sample fetching
        let specific_param = fetchSpecificParam(i);

        // discard the useless data
        let useful_data = discardUselessData(e);

        // the sample after modification
        let preprocessed_sample = preprocessing(useful_data, global_param, specific_param);

        // the x y projection
        let plot_data = projection(preprocessed_sample, global_param);

        // update all plot data and all colors
        all_plot_data.push(plot_data);
        all_colors.push(specific_param.color);

        // draw the reference
        drawRef($('canvas').get(0), global_param);

        // plot the data
        plot($('canvas').get(0), plot_data, specific_param.color);

    });

    // add event for animation
    $("#play").unbind().click(function (ev) {
        // clear the canvas
        let cv = $('canvas').get(0);
        let ctx = cv.getContext("2d");
        ctx.clearRect(0, 0, width, height);
        drawRef(cv, global_param);

        // play the animation
        play(cv, all_plot_data, all_colors);
    })
}

// play an animation for all samples
function play(cv, data, colors) {
    // disable inputs
    $("input").prop("disabled", true);

    // get context
    let ctx = cv.getContext("2d");
    ctx.lineWidth = 1;

    // number of samples
    let nb_sample = data.length;

    // indexes for each sample
    let indexes = new Array();
    for (let i = 0; i < nb_sample; i++) {
        indexes[i] = 0;
    }

    // shortest and longest time
    let max_t = 0;
    let min_t = 0;
    data.forEach(function (e, i) {
        let tmp_1 = e.t[e.t.length - 1];
        if (tmp_1 > max_t) {
            max_t = tmp_1;
        };
        let tmp_2 = e.t[0];
        if (tmp_2 < min_t) {
            min_t = tmp_2;
        }
    });

    // animation is done here
    // i is a common iterator for all samples
    let i = min_t;
    interval = setInterval(function () {

        // stop when we reach the maximum time
        if (i > max_t) {
            stopPlay();
            return;
        }

        data.forEach(function (e, j) {

            // if a time t is equal to i
            // we plot the data, and increment the respective iterator
            if (e.t[indexes[j]] == i) {
                indexes[j]++;
                let plot_x = middle_width + (e.x[indexes[j]]);
                let plot_y = middle_height + (e.y[indexes[j]]);

                // draw the line
                ctx.strokeStyle = colors[j];
                if (indexes[j] > 0) {
                    ctx.beginPath();
                    ctx.moveTo(plot_x, plot_y);
                    ctx.lineTo(middle_width + e.x[indexes[j] - 1], middle_height + e.y[indexes[j] - 1]);
                    ctx.stroke();
                }

                // draw the point
                ctx.fillStyle = "#000000";
                ctx.fillRect(plot_x - 3, plot_y - 3, 6, 6);

                ctx.fillStyle = colors[j];
                ctx.fillRect(plot_x - 2, plot_y - 2, 4, 4);
            }
        });
        i++;
    }, 1);
}

function stopPlay() {
    // enabled inputs
    $("input").prop("disabled", false);
    // stop the animation
    clearInterval(interval);
}

// https://en.wikipedia.org/wiki/3D_projection
function projection(t, param) {

    // the result
    let res = {
        "x": new Array(),
        "y": new Array(),
        "t": new Array()
    }
    // coordinates of the camera
    let c_x = param.camera_position_x;
    let c_y = param.camera_position_y;

    let len = t.position.x.value.length;
    for (let i = 0; i < len; i++) {


        res.x.push(t.position.x.value[i] + c_x);
        res.y.push(t.position.y.value[i] + c_y);
    }

    res.t = copy(t.position.x.interval);
    return res;
}

// cv : which canvas
// t : the data of 1 sample, which contain two array of x and y value and their intervals
// color : the color
function plot(cv, t, color) {
    // context
    let ctx = cv.getContext("2d");
    ctx.beginPath();
    ctx.lineWidth = 1;
    // define color
    ctx.strokeStyle = color;

    // move to the first point
    ctx.moveTo(middle_width + t.x[0], middle_height + t.y[0]);

    // draw
    t.x.forEach(function (e, i) {
        // draw line
        ctx.lineTo(
            middle_width + e,
            middle_height + t.y[i]
        );
        ctx.stroke();

        // draw point
        ctx.fillStyle = "#000000";
        ctx.fillRect(middle_width + e - 3, middle_height + t.y[i] - 3, 6, 6);
        ctx.fillStyle = color;
        ctx.fillRect(middle_width + e - 2, middle_height + t.y[i] - 2, 4, 4);

    })
}

// draw a reference in the 3D world, basically draw the 3 axis
function drawRef(cv, param) {

    // size of the line representing each axis
    var point_ref = 10 * param.coeff_space;
    // dummy data to represent the reference
    var reference = {
        "position": {
            "x":
            {
                "value": [0, point_ref, 0, 0],
                "interval": []
            },
            "y": {
                "value": [0, 0, point_ref, 0],
                "interval": []
            },
            "z": {
                "value": [0, 0, 0, point_ref],
                "interval": []
            }
        }
    };

    // dummy parameters
    let param_ref = {
        "space_shift_x": 0,
        "space_shift_y": 0,
        "space_shift_z": 0,
        // scale in space for x, y, z
        "space_scale_x": 1,
        "space_scale_y": 1,
        "space_scale_z": 1,
        // shift in time
        "time_shift": 0,
        // scale in time
        "time_scale": 1
    };

    // preprocessing the ref
    let preprocessed_ref = preprocessing(reference, param, param_ref);
    // project the ref
    let proj_ref = projection(preprocessed_ref, param);

    // draw the ref
    let ctx = cv.getContext("2d");
    ctx.lineWidth = 2;

    ctx.beginPath();
    ctx.moveTo(middle_width + proj_ref.x[0], middle_height + proj_ref.y[0]);
    ctx.strokeStyle = "#FF0000";
    ctx.lineTo(middle_width + proj_ref.x[1], middle_height + proj_ref.y[1]);
    ctx.stroke();

    ctx.beginPath();
    ctx.moveTo(middle_width + proj_ref.x[0], middle_height + proj_ref.y[0]);
    ctx.strokeStyle = "#00FF00";
    ctx.lineTo(middle_width + proj_ref.x[2], middle_height + proj_ref.y[2]);
    ctx.stroke();

    ctx.beginPath();
    ctx.moveTo(middle_width + proj_ref.x[0], middle_height + proj_ref.y[0]);
    ctx.strokeStyle = "#0000FF";
    ctx.lineTo(middle_width + proj_ref.x[3], middle_height + proj_ref.y[3]);
    ctx.stroke();
}