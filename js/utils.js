// script that offer useful function for other scripts

// transform a form to json
function formToJSON(form) {
    var data = new FormData(form);
    var res = {};
    data.forEach(function (v, k) {
        res[k] = v;
    })
    return res;
}

// source : https://stackoverflow.com/questions/951021/what-is-the-javascript-version-of-sleep
// sent data through ajax
function ajaxSend(url, data, method, functionSuccess, functionError) {
    $.ajax({
        type: method,
        url: url,
        dataType: "json",
        data: data,
        xhrFields: {
            withCredentials: true
        },
        success: function (answer) {
            if (answer.status === "ok") {
                // the server did respond normally, and the answer is correct
                functionSuccess(answer.result);
            } else {
                // the server did respond normally, but the answer is an error
                functionError(answer.message);
            }
        },
        // in case of unexpected behaviour
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(thrownError);
        }
    });
}

// sent form data to ajax
function ajaxSendForm(form, method, functionSuccess, functionError) {
    ajaxSend(form.action, formToJSON(form), method, functionSuccess, functionError);
}

// random number
function randBetween(a, b) {
    return Math.random() * (b - a) + a;
}

// convert a hue to rgb : https://www.rapidtables.com/convert/color/hsv-to-rgb.html
// h : hue between 0 and 360
// s : saturation between 0 and 1
// b : brightness between 0 and 1
function HUEtoRGB(h, s, v) {
    let c = v * s;
    let x = c * (1 - Math.abs(((h / 60) % 2) - 1));
    let m = v - c;

    let tmp_r;
    let tmp_g;
    let tmp_b;

    if (h < 60) {
        tmp_r = c;
        tmp_g = x;
        tmp_b = 0;
    } else if (h < 120) {
        tmp_r = x;
        tmp_g = c;
        tmp_b = 0;
    } else if (h < 180) {
        tmp_r = 0;
        tmp_g = c;
        tmp_b = x;
    } else if (h < 240) {
        tmp_r = 0;
        tmp_g = x;
        tmp_b = c;
    } else if (h < 300) {
        tmp_r = x;
        tmp_g = 0;
        tmp_b = c;
    } else {
        tmp_r = c;
        tmp_g = 0;
        tmp_b = x;
    }
    let res = [];
    res[0] = (tmp_r + m) * 255;
    res[1] = (tmp_g + m) * 255;
    res[2] = (tmp_b + m) * 255;
    return threeValToRGBString(res[0], res[1], res[2]);
}

// rgb to string
function threeValToRGBString(r, g, b) {
    let res = "#";
    res += decToHex(r);
    res += decToHex(g);
    res += decToHex(b);
    return res;
}

// decimal to hexadecimal
function decToHex(r) {
    return Math.floor(r).toString(16);
}

// make a copy of e
function copy(e) {
    return JSON.parse(JSON.stringify(e));
}

// choose object a, b, or c according to i
function axisSelector(i, a, b, c) {
    let tmp = i % 3;
    if (tmp == 0) {
        return a;
    } else {
        if (tmp == 1) {
            return b;
        } else {
            return c;
        }
    }
}

// from : https://stackoverflow.com/questions/1484506/random-color-generator
function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}