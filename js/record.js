// objects that will contain the data
// keyword to be sent
var current_keywords = new Array();

// accelerometer data
var movement = {
    "x": { "value": new Array(), "interval": new Array() },
    "y": { "value": new Array(), "interval": new Array() },
    "z": { "value": new Array(), "interval": new Array() }
};

// orientation data
var rotation = {
    "pitch": { "value": new Array(), "interval": new Array() },
    "roll": { "value": new Array(), "interval": new Array() },
    "yaw": { "value": new Array(), "interval": new Array() }
};

window.addEventListener("load", function () {
    // event for starting the recording
    $("#record").click(countdownToRecording);

    // keyword manipulation
    $("#add_keyword").click(function (ev) {
        var new_kw = $("#keyword_input").val();
        if (new_kw.length >= 1) {
            $("#keyword_input").val("");
            current_keywords.push(new_kw);
            $("#current_keywords").append(new_kw + " ");
        }
    });

    // clear all the keywords
    $("#clear_keyword").click(function (ev) {
        current_keywords = new Array();
        $("#current_keywords").text("Mots clés actuels : ");
    });

    // link to search client
    $("#to_search").click(function(ev) {
        document.location.href = "search.php";
    })
});

// populate the movement global variable
function storeAcceleration(ev) {
    let interval = Date.now() - now;
    movement.x.value.push(ev.acceleration.x);
    movement.y.value.push(ev.acceleration.y);
    movement.z.value.push(ev.acceleration.z);


    // how much time has passed between each moment the data has been captured, in milliseconds
    movement.x.interval.push(interval);
    movement.y.interval.push(interval);
    movement.z.interval.push(interval);

}

// populate the orientation global variable
function storeOrientation(ev) {

    let interval = Date.now() - now;
    rotation.pitch.value.push(ev.beta);
    rotation.roll.value.push(ev.gamma);
    rotation.yaw.value.push(ev.alpha);

    // how much time has passed between each moment the data has been captured, in milliseconds
    rotation.pitch.interval.push(interval);
    rotation.roll.interval.push(interval);
    rotation.yaw.interval.push(interval);
}

// create a countdown to recording
function countdownToRecording(ev) {
    // clear success text
    $("#recording_status").text("");

    // get the button
    var record = $("#record");
    record.unbind("click");
    record.text(3);
    setTimeout(function () {
        record.text(2);
        setTimeout(function () {
            record.text(1);
            setTimeout(activeRecording, 1000);
        }, 1000)
    }, 1000);
}

// function that handles when the recording is being recorded
function activeRecording() {
    // get the button
    var record = $("#record");

    // once the recording start, change the text to stop so if pressed the recording stop
    record[0].textContent = "Arrêter l'enregistrement";

    // remove other event handler, such as stop downloading
    record.unbind("click");

    // add an event handler when recording stops
    record.click(function (ev) {

        // remove the event handlers for acceleration and orientation
        window.removeEventListener("devicemotion", storeAcceleration);
        window.removeEventListener("deviceorientation", storeOrientation);

        // upload the recording
        uploadRecording();

        // put the text back to record
        record[0].textContent = "Démarrer l'enregistrement";

        // remove this event handler
        record.unbind("click");

        // reset the orientation and movement variables
        movement.x = { "value": new Array(), "interval": new Array() };
        movement.y = { "value": new Array(), "interval": new Array() };
        movement.z = { "value": new Array(), "interval": new Array() };

        rotation.pitch = { "value": new Array(), "interval": new Array() };
        rotation.roll = { "value": new Array(), "interval": new Array() };
        rotation.yaw = { "value": new Array(), "interval": new Array() };

        // re add the record event handler
        record.click(countdownToRecording);
    });

    // set the time of the recording, this will be used for computing how much time passed between the start of the recording and
    // and the moment the acceleration has been registered, for each entry
    now = Date.now();

    // event handlers to recording movement and orientation
    window.addEventListener("devicemotion", storeAcceleration);
    window.addEventListener("deviceorientation", storeOrientation);
}

function uploadRecording() {
    // define the username
    let user = $("body").attr("data-id");
    if (user.length <= 0) {
        // if non existent, he's a guest
        user = "Invité";
    }

    // define the date of the end of the recording
    let today = new Date();
    let date = today.getFullYear() + '-' + (today.getMonth()+1) + '-' + today.getDate();

    // define the keywords
    let keywords = current_keywords;

    // define the acceleration
    let x = movement.x;
    let y = movement.y;
    let z = movement.z;

    // define the orientation
    let pitch = rotation.pitch;
    let roll = rotation.roll;
    let yaw = rotation.yaw;

    let data =
    {
        "user": user,
        "x": x,
        "y": y,
        "z": z,
        "pitch": pitch,
        "roll": roll,
        "yaw": yaw,
        "date": date,
        "keywords": keywords,
    };

    data = {
        "data": JSON.stringify(data)
    };

    ajaxSend("reponses/addRecording.php", data, "POST",
        function (res) {
            $("#recording_status").text("Enregistrement avec succès");
        },
        function (res) {
            $("#recording_status").text("Enregistrement échoué, veuillez reessayer");
        });
}
